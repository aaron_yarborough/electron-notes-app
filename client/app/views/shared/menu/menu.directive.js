const remote = require('electron').remote

angular.module('app').controller('menuDirectiveController', [
    '$scope',
    function ($scope) {
        $scope.exportAsHtml = function() {

        }

        $scope.exit = function() {
            let currWindow = remote.getCurrentWindow()
            currWindow.close()
        }
    }]
).directive('menu', function() {
    return {
        templateUrl: 'views/shared/menu/menu.tpl.html'
    }
})
